class Zombie < ApplicationRecord
	has_many :brains
	belongs_to :user
	mount_uploader :avatar, AvatarUploader
	validates :bio, length: { maximum: 100 }
	validates :name, presence: true
	validates :age, numericality: {only_integer: true, message: "Solo numeros enteros"}
	validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,message: "formato mal"}
end
